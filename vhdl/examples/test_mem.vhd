use ternary.all;

entity main is
    Port ( s : in FakeTrit;
           n : in FakeTrit;
           p : in FakeTrit;
           q : out FakeTrit);
end main;

architecture Behavioral of main is

begin

mem1: ternary_mem port map( T_S => s, T_N => n, T_P => p, T_Q => q);

end Behavioral;
