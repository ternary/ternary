// TriClock.java - Alexander Shabarshin  14.03.2005-15.03.2005

import java.applet.*;
import java.util.*;
import java.awt.*;

public class TriClock extends Applet implements Runnable
{
 Thread thread = null;
 Date data = null;
 int[] time;

 public void init()
 {
   setBackground(new Color(255,255,255));
   time = new int[3];
 }
   
 public void paint(Graphics g)
 {
   int i,j,k,n,m,x,y;
   int dx = size().width;
   int dy = size().height;
   int st = dx/9;
   time[0] = data.getHours();
   time[1] = data.getMinutes();
   time[2] = data.getSeconds();
   if(time[2]>=30){time[2]-=60;time[1]++;}
   if(time[1]>=30){time[1]-=60;time[0]++;}
   if(time[0]>=24){time[0]-=24;}
   showStatus("Time="+time[0]+":"+time[1]+":"+time[2]);
   for(j=0;j<3;j++)
   {
     k = 27;
     for(i=0;i<4;i++)
     {
       n = time[j]/k;
       time[j] -= n*k;
       m = k/2;
       if(time[j]>+m){n++;time[j]-=k;}
       if(time[j]<-m){n--;time[j]+=k;}
       switch(n)
       {
         case -1: g.setColor(new Color(0xFF,0x00,0x00)); break;
         case  0: g.setColor(new Color(0xFF,0xFF,0xFF)); break;
         case  1: g.setColor(new Color(0x00,0xFF,0x00)); break;
       }   
       x = st + i*st*2;
       y = st + j*st*2;
       g.fillOval(x,y,st,st);
       g.setColor(new Color(0,0,0));
       g.drawOval(x,y,st,st);
       k /= 3;
     }
   }
 }

 public void update(Graphics g)
 {
   paint(g);
 }

 public void run()
 {
   while(true)
   {
      try {
        data = new Date();
        repaint();
        thread.sleep(250);
      } catch(Exception e) {
        System.out.println("Exception "+e);
      }   
   }
 }

 public void start()
 {
   if(thread==null)
   {
      thread = new Thread(this);
      thread.start();
   }
 }

 public void stop()
 {
   if(thread!=null)
   {
      thread.stop();
      thread = null;
   }
 }

} 
