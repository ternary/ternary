// TriADC1.java - Alexander Shabarshin  12.02.2006

public class TriADC1
{
 static double co[] = new double[4];
 static double u[] = new double[5];

 static double ti(double u)
 {
//  System.out.println(" ti "+u);
  if(u > +2.5) return 0;//-5;
//  if(u < -2.5) return +5;
  return 5;//0;
 }

 static double fun(int n, double u1) 
 { 
  double sum = 0;
  double res = 0;
  u[4] = u1; // trick
  for(int i=0;i<n;i++) sum+=co[i];
  for(int i=0;i<n;i++) res+=co[n-i-1]*u[4-i]/sum;
  return res; 
 }
 
 public static void main(String arg[])
 {
  co[0] = 1;
  co[1] = 2;
  co[2] = 4;
  co[3] = 8;

  double U = 0;
  double Us = 0.05;

  while(U<=5.0)
  {
   u[3] = ti(fun(1,U));
   u[2] = ti(fun(2,U));
   u[1] = ti(fun(3,U));
   u[0] = ti(fun(4,U));
   String uval = "";
   for(int i=3;i>=0;i--)
   {
     uval += " "+u[i];
   }
   System.out.println("U=" + U + " ADC =" + uval);
   U += Us;
  }
  
 }
 
}
