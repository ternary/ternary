// Schemes.java - Alexander Shabarshin  02.04.2005

import java.applet.*;
import java.awt.*;

public class Schemes extends Applet
{
 static final int scheme = 3;
 static final int cmax = 100;
 int[] xs = new int[cmax];
 int[] ys = new int[cmax];
 Graphics g = null;

 public void drawText(int x, int y, String text)
 {
   g.drawString(text,x,y);
 }

 public void drawVec(int x, int y, int dx, int dy)
 {
   g.drawLine(x,y,x+dx,y+dy);
 }

 public void drawHor(int x, int y, int l)
 {
   while(l>20) 
   {
     drawVec(x,y,10,0);
     l -= 20;
     x += 20;
   }
   drawVec(x,y,l,0);
 }

 public void drawVer(int x, int y, int l)
 {
   while(l>20) 
   {
     drawVec(x,y,0,10);
     l -= 20;
     y += 20;
   }
   drawVec(x,y,0,l);
 }

 public void drawDot(int x, int y)
 {
   g.fillOval(x-3,y-3,7,7);
 }

 public void drawPin(int x, int y)
 {
   g.drawOval(x-5,y-5,10,10);
 }

 // Height=20
 public void drawVpp(int x, int y)
 {
   drawVec(x,y,0,20);
   drawVec(x,y,5,10);
   drawVec(x,y,-5,10);
 }

 // Height=20
 public void drawGnd(int x, int y)
 {
   drawVec(x,y,0,20);
   drawVec(x-10,y+20,20,0);
 }

 // Height=20
 public void drawVmm(int x, int y)
 {
   drawVec(x,y,0,20);
   drawVec(x,y+20,5,-10);
   drawVec(x,y+20,-5,-10);
 }

 // Height=70
 public void drawRes(int x, int y)
 {
   g.drawLine(x,y,x,y+20);
   xs[0]=x-5;	ys[0]=y+20;
   xs[1]=x+5;	ys[1]=y+20;
   xs[2]=x+5;	ys[2]=y+50;
   xs[3]=x-5;	ys[3]=y+50;
   g.drawPolygon(xs,ys,4);
   g.drawLine(x,y+50,x,y+70);
 }

 // Height=60
 public void drawDiU(int x, int y)
 {
   g.drawLine(x,y,x,y+60);
   xs[0]=x;	ys[0]=y+20;
   xs[1]=x+10;	ys[1]=y+40;
   xs[2]=x-10;	ys[2]=y+40;
   g.drawPolygon(xs,ys,3);
   g.drawLine(x-10,y+20,x+10,y+20);
 }

 // Height=60
 public void drawDiD(int x, int y)
 {
   g.drawLine(x,y,x,y+60);
   xs[0]=x;	ys[0]=y+40;
   xs[1]=x+10;	ys[1]=y+20;
   xs[2]=x-10;	ys[2]=y+20;
   g.drawPolygon(xs,ys,3);
   g.drawLine(x-10,y+40,x+10,y+40);
 }

 // Height=20 Width=60
 public void drawDiL(int x, int y)
 {
   g.drawLine(x,y,x+60,y);
   xs[0]=x+20;	ys[0]=y;
   xs[1]=x+40;	ys[1]=y-10;
   xs[2]=x+40;	ys[2]=y+10;
   g.drawPolygon(xs,ys,3);
   g.drawLine(x+20,y+10,x+20,y-10);
 }

 // Height=20 Width=60
 public void drawDiR(int x, int y)
 {
   g.drawLine(x,y,x+60,y);
   xs[0]=x+40;	ys[0]=y;
   xs[1]=x+20;	ys[1]=y-10;
   xs[2]=x+20;	ys[2]=y+10;
   g.drawPolygon(xs,ys,3);
   g.drawLine(x+40,y+10,x+40,y-10);
 }

 public void drawOpa(int x, int y)
 {
   xs[0]=x;	ys[0]=y;
   xs[1]=x+100;	ys[1]=y+50;
   xs[2]=x;	ys[2]=y+100;
   g.drawPolygon(xs,ys,3);
   g.drawLine(x+10,y+25,x+20,y+25);
   g.drawLine(x+10,y+75,x+20,y+75);
   g.drawLine(x+15,y+70,x+15,y+80);
   g.drawLine(x-20,y+25,x,y+25);
   g.drawLine(x-20,y+75,x,y+75);
   g.drawLine(x+100,y+50,x+120,y+50);
 }

 public void drawCmp(int x, int y)
 {
   drawOpa(x,y);
   drawDot(x+120,y+50);
   drawVpp(x+120,y-40);
   drawText(x+125,y-35,"+5V");
   drawRes(x+120,y-20);
   drawVec(x+120,y+50,20,0);
 }

 public void paint(Graphics gg)
 {
   g = gg;
   g.setColor(new Color(0,0,0));
   g.setFont(new Font("Arial",Font.PLAIN,16));
   switch(scheme)
   {
     case 1:
	drawVpp(80,55);
	drawText(85,60,"+5V");
	drawCmp(100,100);
	drawVec(240,150,20,0);
	drawRes(80,55);
	drawVec(80,125,0,50);
	drawRes(80,175);
	drawVec(80,245,0,10);
	drawDot(80,250);
	drawVec(80,250,40,0);
	drawGnd(120,250);
	drawCmp(100,300);
	drawVec(240,350,20,0);
	drawRes(80,255);
	drawVec(80,325,0,50);
	drawRes(80,375);
	drawVmm(80,425);
	drawText(85,450,"-5V");
	drawDot(80,125);
	drawDot(80,325);
	drawVec(80,175,-50,0);
	drawVec(80,375,-50,0);
	drawVec(30,175,0,200);
	drawDot(30,250);
	drawVec(30,250,-15,0);
	drawPin(10,250);
	drawText(5,240,"IN");
	drawDot(280,150);
	drawDot(280,350);
	drawDiD(280,150);
	drawDiD(280,290);
	drawVec(280,290,0,-80);
//	drawDiR(280,150);
//	drawDiR(280,350);
	drawDot(280,250);
	drawVec(280,250,100,0);
	drawVec(260,150,120,0);
	drawVec(260,350,120,0);
//	drawDot(340,150);
//	drawRes(340,150);
//	drawGnd(340,200);
	drawDot(340,250);
	drawRes(340,250);
	drawGnd(340,300);
//	drawDot(340,350);
//	drawRes(340,350);
//	drawGnd(340,400);	
	drawPin(385,150);
	drawPin(385,250);
	drawPin(385,350);	
	drawText(395,150,"EQP");
	drawText(395,250,"BUF");
	drawText(395,350,"NEN");
	break;

     case 2:
	drawVpp(80,55);
	drawText(85,60,"+5V");
	drawCmp(100,100);
	drawVec(240,150,20,0);
	drawRes(80,55);
	drawVec(80,125,0,50);
	drawRes(80,175);
	drawVec(80,245,0,10);
	drawDot(80,250);
	drawVec(80,250,40,0);
	drawGnd(120,250);
	drawCmp(100,300);
	drawVec(240,350,20,0);
	drawRes(80,255);
	drawVec(80,325,0,50);
	drawRes(80,375);
	drawVmm(80,425);
	drawText(85,450,"-5V");
	drawDot(80,175);
	drawDot(80,375);
	drawVec(80,125,-50,0);
	drawVec(80,325,-50,0);
	drawVec(30,125,0,200);
	drawDot(30,250);
	drawVec(30,250,-15,0);
	drawPin(10,250);
	drawText(5,240,"IN");
	drawDot(280,150);
	drawDot(280,350);
	drawDiU(280,150);
	drawDiU(280,290);
	drawVec(280,290,0,-80);
//	drawDiR(280,150);
//	drawDiR(280,350);
	drawDot(280,250);
	drawVec(280,250,100,0);
	drawVec(260,150,120,0);
	drawVec(260,350,120,0);
//	drawDot(340,150);
//	drawRes(340,150);
//	drawGnd(340,200);
	drawDot(340,250);
	drawRes(340,250);
	drawGnd(340,300);
//	drawDot(340,350);
//	drawRes(340,350);
//	drawGnd(340,400);
	drawPin(385,150);
	drawPin(385,250);
	drawPin(385,350);
	drawText(395,150,"NEP");
	drawText(395,250,"INV");
	drawText(395,350,"EQN");
	break;

     case 3:
	drawVpp(80,55);
	drawText(85,60,"+5V");
	drawCmp(100,100);
	drawVec(240,150,20,0);
	drawRes(80,55);
	drawVec(80,125,0,50);
	drawRes(80,175);
	drawVec(80,245,0,10);
	drawDot(80,250);
	drawVec(80,250,40,0);
	drawGnd(120,250);
	drawCmp(100,300);
	drawVec(240,350,20,0);
	drawRes(80,255);
	drawVec(80,325,0,50);
	drawRes(80,375);
	drawVmm(80,425);
	drawText(85,450,"-5V");
	drawDot(80,125);
	drawDot(80,375);
	drawVec(80,175,-50,0);
	drawVec(80,325,-50,0);
	drawVec(30,175,0,150);
	drawDot(30,250);
	drawVec(30,250,-15,0);
	drawPin(10,250);
	drawText(5,240,"IN");
	drawDot(280,150);
	drawDot(280,350);
	drawDiD(280,150);
	drawDiU(280,290);
	drawVec(280,290,0,-80);
	drawDot(280,250);
	drawVec(280,250,100,0);
	drawVec(260,150,120,0);
	drawVec(260,350,120,0);
	drawDot(340,250);
	drawRes(340,250);
	drawGnd(340,300);
	drawPin(385,150);
	drawPin(385,250);
	drawPin(385,350);	
	drawText(395,150,"EQP");
	drawText(395,250,"NEO");
	drawText(395,350,"EQN");
	break;

     case 10:
	break;

     case 11:
        drawVec(100,100,200,0);
        drawVec(300,100,0,200);
        drawVec(300,300,-200,0);
        drawVec(100,300,0,-200);
        drawText(110,120,"DG403");
        drawPin(75,150);
        drawPin(75,200);
        drawPin(75,250);
        drawVec(80,150,140,0);
        drawVec(80,200,50,0);
        drawVec(80,250,50,0);
        drawVec(130,200,30,-10);
        drawVec(160,200,0,-5);
        drawVec(160,200,30,0);
        drawVec(130,250,30,-10);
        drawVec(160,250,30,0);
        drawVec(190,200,0,50);
        drawDot(190,225);
        drawVec(190,225,30,0);
        drawVec(220,150,30,-10);
        drawVec(220,225,30,-10);
        drawVec(250,150,0,-5);
        drawVec(250,150,30,0);
        drawVec(250,225,30,0);
        drawVec(280,150,0,75);
        drawDot(280,200);
        drawVec(280,200,40,0);
        drawPin(325,200);
        drawVer(145,200,125);
        drawVer(235,150,175);
        drawPin(145,330);
        drawPin(235,330);
        drawText(70,140,"RN");
        drawText(70,190,"RO");
        drawText(70,240,"RP");
        drawText(310,190,"OUT");
        drawText(130,355,"EQP");
        drawText(220,355,"NEN");
	break;

   }	
 }

}
