// Alpha2.java - Alexander Shabarshin  12.11.2008
// version 2.1 ( see http://3niti.org )
// based on Alpha1.java (28.10.2008 - 12.11.2008)
// 13.11.2008 (v2.0) - initial version
// 07.12.2008 (v2.1) - read and write operations

import java.applet.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class Alpha2 extends Applet implements Runnable
{

 Thread thread;
 AlphaPanel panel;
 Alpha2s middle,bottom;

 public void init()
 {
   panel = new AlphaPanel();
   setLayout(new GridLayout(3,1));
   add(panel);
   middle = new Alpha2s(panel,1);
   add(middle);
   bottom = new Alpha2s(panel,2);
   add(bottom);
 }
   
 public void start()
 {
   if(thread==null)
   {
      thread = new Thread(this);
      thread.start();
   }
 }

 public void stop()
 {
   if(thread!=null)
   {
      thread.stop();
      thread = null;
   }
 }

 public void run()
 {
   while(true)
   {
      panel.run();
      try {
        thread.sleep(200);
      } catch(Exception e) {
        System.out.println("Exception "+e);
      }   
   }
 }

} 

class Alpha2s extends Panel implements AlphaListener, AlphaProgram
{
 AlphaPanel alpha;
 int frame, width, height;
 TextArea tProgram;
 TextField tDN,tDO,tDP,tPC,tN,tO,tP,tF,tA,tB,tL,tM,tH,tUser;
 Button bDN,bDO,bDP,bPC,bStep,bWrite,bRead,bN,bO,bP,bF,bA,bB,bL,bM,bH,bNew;

 public Alpha2s(AlphaPanel a, int f)
 {
  alpha = a;
  frame = f;
  String r = " Register ";
  if(frame==1 || frame==2)
  {
    setLayout(new GridLayout(1,2));
  }
  else
  {
    setLayout(new GridLayout(5,3));
  }
  if(frame==1)
  {
   Alpha2s a3 = new Alpha2s(alpha,3);
   add(a3);
   a3.init();
   Alpha2s a4 = new Alpha2s(alpha,4);
   add(a4);
   a4.init();
  }
  else if(frame==2)
  {
   tProgram = new TextArea();
   add(tProgram);
   Alpha2s a5 = new Alpha2s(alpha,5);
   add(a5);
   a5.init();
   alpha.core().setProgram(this);
  }
  else if(frame==3)
  {
   add(new Label(r+"DPn:"));
   tDN = new TextField();
   add(tDN);
   bDN = new Button("Clear DPn");
   add(bDN);
   add(new Label(r+"DPo:"));
   tDO = new TextField();
   add(tDO);
   bDO = new Button("Clear DPo");
   add(bDO);
   add(new Label(r+"DPp:"));
   tDP = new TextField();
   add(tDP);
   bDP = new Button("Clear DPp");
   add(bDP);
   add(new Label(r+"PC:"));
   tPC = new TextField();
   add(tPC);
   bPC = new Button("Clear PC");
   add(bPC);
   bStep = new Button("Step");
   add(bStep);
   bRead = new Button("Read");
   add(bRead);
   bWrite = new Button("Write");
   add(bWrite);
  }
  else if(frame==4)
  {
   add(new Label(r+"N:"));
   tN = new TextField();
   add(tN);
   bN = new Button("Clear N");
   add(bN);
   add(new Label(r+"O:"));
   tO = new TextField();
   add(tO);
   bO = new Button("Clear O");
   add(bO);
   add(new Label(r+"P:"));
   tP = new TextField();
   add(tP);
   bP = new Button("Clear P");
   add(bP);
   add(new Label(r+"F:"));
   tF = new TextField();
   add(tF);
   bF = new Button("Clear F");
   add(bF);
   add(new Label(r+"A:"));
   tA = new TextField();
   add(tA);
   bA = new Button("Clear A");
   add(bA);
  }
  else if(frame==5)
  {
   add(new Label(r+"B:"));
   tB = new TextField();
   add(tB);
   bB = new Button("Clear B");
   add(bB);
   add(new Label(r+"L:"));
   tL = new TextField();
   add(tL);
   bL = new Button("Clear L");
   add(bL);
   add(new Label(r+"M:"));
   tM = new TextField();
   add(tM);
   bM = new Button("Clear M");
   add(bM);
   add(new Label(r+"H:"));
   tH = new TextField();
   add(tH);
   bH = new Button("Clear H");
   add(bH);
   add(new Label(" Username:"));
   tUser = new TextField();
   add(tUser);
   bNew = new Button("New Session");
   add(bNew);
  }
 }

 public void paint(Graphics g)
 {
  width = size().width;
  height = size().height;
 }

 public void init()
 {
  alpha.core().addListener(this);
 }

 public static Ternary parse(String s)
 {
  Ternary t;
  try
  {
    int i = Integer.parseInt(s);
    if(i < -13) i = -13;
    if(i >  13) i =  13;
    t = new Ternary(i); 
  }
  catch(Exception e)
  {
    t = new Ternary(s);
  }
  return t;
 }

 public boolean action(Event event, Object object)
 {
  int i;
  String s;
  if(event.target instanceof Button)
  {
    Button b = (Button)event.target;
    if(bStep!=null && bStep.equals(b))
    {
      alpha.step();
    }
    else if(bNew!=null && bNew.equals(b))
    {
      alpha.core().cleanProgram();
    }
    else if(bRead!=null && bRead.equals(b))
    {
      alpha.core().copyProgram();
    }
    else if(bWrite!=null && bWrite.equals(b))
    {
      alpha.core().assembleProgram();
    }
    else if(bPC!=null && bPC.equals(b))
    {
      alpha.core().setPC(0);
    }
    else if(bDN!=null && bDN.equals(b))
    {
      alpha.core().setD(Ternary.N, new Ternary(0));
    }
    else if(bDO!=null && bDO.equals(b))
    {
      alpha.core().setD(Ternary.O, new Ternary(0));
    }
    else if(bDP!=null && bDP.equals(b))
    {
      alpha.core().setD(Ternary.P, new Ternary(0));
    }
    else if(bN!=null && bN.equals(b))
    {
      alpha.core().setN(new Ternary(0));
    }
    else if(bO!=null && bO.equals(b))
    {
      alpha.core().setO(new Ternary(0));
    }
    else if(bP!=null && bP.equals(b))
    {
      alpha.core().setP(new Ternary(0));
    }
    else if(bF!=null && bF.equals(b))
    {
      alpha.core().setF(new Ternary(0));
    }
    else if(bA!=null && bA.equals(b))
    {
      alpha.core().setA(new Ternary(0));
    }
    else if(bB!=null && bB.equals(b))
    {
      alpha.core().setB(new Ternary(0));
    }
    else if(bL!=null && bL.equals(b))
    {
      alpha.core().setL(new Ternary(0));
    }
    else if(bM!=null && bM.equals(b))
    {
      alpha.core().setM(new Ternary(0));
    }
    else if(bH!=null && bH.equals(b))
    {
      alpha.core().setH(new Ternary(0));
    }
  }
  if(event.target instanceof TextField)
  {
    TextField x = (TextField)event.target;
    if(tPC!=null && tPC.equals(x))
    {
      Ternary t = new Ternary(tPC.getText());
      alpha.core().setPC(t.get());
    }
    else if(tDN!=null && tDN.equals(x))
    {
      alpha.core().setD(Ternary.N, new Ternary(tDN.getText()));
    }
    else if(tDO!=null && tDO.equals(x))
    {
      alpha.core().setD(Ternary.O, new Ternary(tDO.getText()));
    }
    else if(tDP!=null && tDP.equals(x))
    {
      alpha.core().setD(Ternary.P, new Ternary(tDP.getText()));
    }
    else if(tN!=null && tN.equals(x))
    {
      alpha.core().setN(parse(tN.getText()));
    }
    else if(tO!=null && tO.equals(x))
    {
      alpha.core().setO(parse(tO.getText()));
    }
    else if(tP!=null && tP.equals(x))
    {
      alpha.core().setP(parse(tP.getText()));
    }
    else if(tF!=null && tF.equals(x))
    {
      alpha.core().setF(parse(tF.getText()));
    }
    else if(tA!=null && tA.equals(x))
    {
      alpha.core().setA(parse(tA.getText()));
    }
    else if(tB!=null && tB.equals(x))
    {
      alpha.core().setB(parse(tB.getText()));
    }
    else if(tL!=null && tL.equals(x))
    {
      alpha.core().setL(parse(tL.getText()));
    }
    else if(tM!=null && tM.equals(x))
    {
      alpha.core().setM(parse(tM.getText()));
    }
    else if(tH!=null && tH.equals(x))
    {
      alpha.core().setH(parse(tH.getText()));
    }
  }
  return true;
 }

 public void event(int i, Ternary t)
 {
   switch(i)
   {
     case AlphaListener.N:
          if(tN!=null) tN.setText(t.toString(3));
          break;
     case AlphaListener.O:
          if(tO!=null) tO.setText(t.toString(3));
          break;
     case AlphaListener.P:
          if(tP!=null) tP.setText(t.toString(3));
          break;
     case AlphaListener.F:
          if(tF!=null) tF.setText(t.toString(3));
          break;
     case AlphaListener.A:
          if(tA!=null) tA.setText(t.toString(3));
          break;
     case AlphaListener.B:
          if(tB!=null) tB.setText(t.toString(3));
          break;
     case AlphaListener.L:
          if(tL!=null) tL.setText(t.toString(3));
          break;
     case AlphaListener.M:
          if(tM!=null) tM.setText(t.toString(3));
          break;
     case AlphaListener.H:
          if(tH!=null) tH.setText(t.toString(3));
          break;
     case AlphaListener.DN:
          if(tDN!=null) tDN.setText(t.toString(9));
          break;
     case AlphaListener.DO:
          if(tDO!=null) tDO.setText(t.toString(9));
          break;
     case AlphaListener.DP:
          if(tDP!=null) tDP.setText(t.toString(9));
          break;
     case AlphaListener.PC:
          if(tPC!=null) tPC.setText(t.toString(9));
          break;
   }
 }

 public String getText()
 {
   if(frame==2)
   {
      return tProgram.getText();
   }
   return "";
 }

 public boolean setText(String t)
 {
   if(frame==2)
   {
      tProgram.setText(t);
      return true;
   }
   return false;
 }

}
