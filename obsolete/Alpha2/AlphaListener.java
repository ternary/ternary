public interface AlphaListener
{
 public static final int N = 1;
 public static final int O = 2;
 public static final int P = 3;
 public static final int F = 4;
 public static final int A = 5;
 public static final int B = 6;
 public static final int L = 7;
 public static final int M = 8;
 public static final int H = 9;
 public static final int DN = 10;
 public static final int DO = 11;
 public static final int DP = 12;
 public static final int PC = 13;
 public void event(int i, Ternary t);
}
