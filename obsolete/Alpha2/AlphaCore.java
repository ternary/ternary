// AlphaCore.java - Alexander Shabarshin  13.11.2008
// version 2.1 ( see http://3niti.org )
// based on Alpha1.java (28.10.2008 - 12.11.2008)
// 13.11.2008 (v2.0) - initial version
// 07.12.2008 (v2.1) - simple assembler and readonly flag

import java.util.*;

public class AlphaCore
{
 int PC,rsf,dpf,bcf;
 Ternary A;
 Ternary B;
 Ternary[] ram;
 Ternary[] rom;
 int[] D;
 public static final int MEM_SZ = 2187;
 public static final int MEM_MX = 1094;
 public static final int MEM_BEG = -MEM_MX;
 public static final int MEM_END = MEM_MX+MEM_SZ;
 boolean readonly = false;
 LinkedList listeners;
 AlphaProgram program;

 public AlphaCore()
 {
   init(false);
 }

 public AlphaCore(boolean ro)
 {
   init(ro);
 }

 void init(boolean ro)
 {
   readonly = ro;
   rom = new Ternary[MEM_SZ];
   ram = new Ternary[MEM_SZ];
   for(int i=0;i<MEM_SZ;i++)
   {
     rom[i] = new Ternary(0);
     ram[i] = new Ternary(0);
   }
   A = new Ternary(0);
   B = new Ternary(0);
   D = new int[3];
   listeners = new LinkedList();
 }

 public void setProgram(AlphaProgram p)
 {
   program = p;
 }

 public void addListener(AlphaListener l)
 {
   listeners.add(l);
   l.event(AlphaListener.N,getN());
   l.event(AlphaListener.O,getO());
   l.event(AlphaListener.P,getP());
   l.event(AlphaListener.F,getF());
   l.event(AlphaListener.A,getA());
   l.event(AlphaListener.B,getB());
   l.event(AlphaListener.L,getL());
   l.event(AlphaListener.M,getM());
   l.event(AlphaListener.H,getH());
   l.event(AlphaListener.DN,getD(Ternary.N));
   l.event(AlphaListener.DO,getD(Ternary.O));
   l.event(AlphaListener.DP,getD(Ternary.P));
   l.event(AlphaListener.PC,new Ternary(PC));
 }

 public void removeListener(AlphaListener l)
 {
   listeners.remove(l);
 }

 void send(int i, Ternary t)
 {
   for(int j=0;j<listeners.size();j++)
   {
      AlphaListener l = (AlphaListener)listeners.get(j);
      l.event(i,t);
   }
 }

 public Ternary get(int a)
 {
   Ternary t;
   if(a >= MEM_MX && a < MEM_END)
   {
      t = ram[a-MEM_MX];
   }
   else if(a > MEM_BEG && a < MEM_MX)
   {
      t = rom[a+MEM_MX-1];
   }
   else 
   {
      t = new Ternary(0);
   }
   return t;
 }

 public void set(int a, Ternary t)
 {
   if(!readonly && a > MEM_BEG && a < MEM_MX)
   {
      rom[a+MEM_MX-1] = new Ternary(t);
   }
   if(a >= MEM_MX && a < MEM_END)
   {
      ram[a-MEM_MX] = new Ternary(t);
   }
   if(a==getDi(Ternary.N)) send(AlphaListener.N,t);
   if(a==getDi(Ternary.O)) send(AlphaListener.O,t);
   if(a==getDi(Ternary.P)) send(AlphaListener.P,t);
 }

 public boolean nearEmpty(int a, int l)
 {
   int n = 0;
   for(int i=a-l;i<a;i++)
   {
      if(!get(i).is(Ternary.OOO))
      {
         n++;
         break;
      }
   }
   for(int i=a+1;i<=a+l;i++)
   {
      if(!get(i).is(Ternary.OOO))
      {
         n++;
         break;
      }
   }
   if(n==2) return false;
   return true;
 }

 public Ternary getA()
 {
   return A;
 }

 public void setA(Ternary t)
 {
   A = t;
   send(AlphaListener.A,t);
 }

 public Ternary getB()
 {
   return B;
 }

 public void setB(Ternary t)
 {
   B = t;
   send(AlphaListener.B,t);
 }

 public Ternary getF()
 {
   return new Ternary(rsf*9+dpf*3+bcf);
 }

 public void setF(Ternary t)
 {
   String s = t.toString();
   int ibcf = s.length() - 1;
   int idpf = ibcf - 1;
   int irsf = idpf - 1;
   if(irsf < 0) rsf = 0;
   else
   {
     switch(s.charAt(irsf))
     {
       case 'N': rsf = -1; break;
       case 'O': rsf =  0; break;
       case 'P': rsf =  1; break;
     }
   }
   if(idpf < 0) dpf = 0;
   else
   {
     switch(s.charAt(idpf))
     {
       case 'N': dpf = -1; break;
       case 'O': dpf =  0; break;
       case 'P': dpf =  1; break;
     }
   }
   switch(s.charAt(ibcf))
   {
     case 'N': bcf = -1; break;
     case 'O': bcf =  0; break;
     case 'P': bcf =  1; break;
   }
   send(AlphaListener.F,t);
   notifyD(dpf,getD());
 }

 void notifyD(int i, Ternary t)
 {
   if(i==0) 
   {
     send(AlphaListener.DO,t);
     send(AlphaListener.O,getO());
   }
   else if(i<0) 
   {
     send(AlphaListener.DN,t);
     send(AlphaListener.N,getN());
   }
   else if(i>0) 
   {
     send(AlphaListener.DP,t);
     send(AlphaListener.P,getP());
   }
   if(i==dpf)
   {
     send(AlphaListener.L,getL());
     send(AlphaListener.M,getM());
     send(AlphaListener.H,getH());
   }
 }

 int getDi()
 {
   return D[dpf+1];
 }

 int getDi(int i)
 {
   return D[i+1];
 }

 Ternary getD()
 {
   return new Ternary(getDi());
 }

 public Ternary getD(int i)
 {
   return new Ternary(D[i+1]);
 }

 void setD(Ternary t)
 {
   D[dpf+1] = t.get();
 }

 void setD(int i)
 {
   D[dpf+1] = i;
 }

 void setD(int h, int m, int l)
 {
   setD(new Ternary(27*(27*h+m)+l));
 }

 public void setD(int i, Ternary t)
 {
   D[i+1] = t.get();
   notifyD(i,t);
 }

 Ternary getTriad(String s, int ll)
 {
   int il = ll - 1;
   int im = il - 1;
   int ih = im - 1;
   int i = 0;
   if(ih>=0)
   {
     if(s.charAt(ih)=='P') i+=9;
     if(s.charAt(ih)=='N') i-=9;
   }
   if(im>=0)
   {
     if(s.charAt(im)=='P') i+=3;
     if(s.charAt(im)=='N') i-=3;
   }
   if(il>=0)
   {
     if(s.charAt(il)=='P') i+=1;
     if(s.charAt(il)=='N') i-=1;
   }
   return new Ternary(i);
 }

 public Ternary getL()
 {
   String s = getD().toString();
   return getTriad(s,s.length());
 }
                                  
 public Ternary getM()
 {
   String s = getD().toString();
   return getTriad(s,s.length()-3);
 }

 public Ternary getH()
 {
   String s = getD().toString();
   return getTriad(s,s.length()-6);
 }

 public void setL(Ternary t)
 {
   setD(getH().get(),getM().get(),t.get());
   send(AlphaListener.L,t);
   notifyD(dpf,getD());
 }

 public void setM(Ternary t)
 {
   setD(getH().get(),t.get(),getL().get());
   send(AlphaListener.M,t);
   notifyD(dpf,getD());
 }
         
 public void setH(Ternary t)
 {
   setD(t.get(),getM().get(),getL().get());
   send(AlphaListener.H,t);
   notifyD(dpf,getD());
 }

 public Ternary getN()
 {
   return get(D[0]);
 }

 public void setN(Ternary t)
 {
   set(D[0],t);
   send(AlphaListener.N,t);
   if(getDi(Ternary.N)==getDi(Ternary.O)) send(AlphaListener.O,t);
   if(getDi(Ternary.N)==getDi(Ternary.P)) send(AlphaListener.P,t);
 }

 public Ternary getO()
 {
   return get(D[1]);
 }

 public void setO(Ternary t)
 {
   set(D[1],t);
   send(AlphaListener.O,t);
   if(getDi(Ternary.O)==getDi(Ternary.N)) send(AlphaListener.N,t);
   if(getDi(Ternary.O)==getDi(Ternary.P)) send(AlphaListener.P,t);
 }

 public Ternary getP()
 {
   return get(D[2]);
 }

 public void setP(Ternary t)
 {
   set(D[2],t);
   send(AlphaListener.P,t);
   if(getDi(Ternary.P)==getDi(Ternary.N)) send(AlphaListener.N,t);
   if(getDi(Ternary.P)==getDi(Ternary.O)) send(AlphaListener.O,t);
 }

 public int getPC()
 {
   return PC;
 }

 public void setPC(int i)
 {
   PC = i;
   send(AlphaListener.PC, new Ternary(PC));
 }

 void opa(Ternary f)
 {
   String s = f.toString();
   int il = s.length() - 1;
   int im = il - 1;
   int ih = im - 1;
   int i = 0;
   int j = 0;
   int k = 0;
   if(ih>=0)
   {
     if(s.charAt(ih)=='P') i =  1;
     if(s.charAt(ih)=='N') i = -1;
   }
   if(im>=0)
   {
     if(s.charAt(im)=='P') j =  1;
     if(s.charAt(im)=='N') j = -1;
   }
   if(il>=0)
   {
     if(s.charAt(il)=='P') k =  1;
     if(s.charAt(il)=='N') k = -1;
   }
   s = A.toString();
   il = s.length() - 1;
   im = il - 1;
   ih = im - 1;
   int r = 0;
   if(ih>=0)
   {
     switch(s.charAt(ih))
     {
       case 'N': r+=9*i; break;
       case 'O': r+=9*j; break;
       case 'P': r+=9*k; break;
     }
   }
   if(im>=0)
   {
     switch(s.charAt(im))
     {
       case 'N': r+=3*i; break;
       case 'O': r+=3*j; break;
       case 'P': r+=3*k; break;
     }
   }
   if(il>=0)
   {
     switch(s.charAt(il))
     {
       case 'N': r+=i; break;
       case 'O': r+=j; break;
       case 'P': r+=k; break;
     }
   }
   A = new Ternary(r);
 }

 void opb(Ternary fn, Ternary fo, Ternary fp)
 {
   String s = fn.toString();
   int il = s.length() - 1;
   int im = il - 1;
   int ih = im - 1;
   int in = 0;
   int jn = 0;
   int kn = 0;
   if(ih>=0)
   {
     if(s.charAt(ih)=='P') in =  1;
     if(s.charAt(ih)=='N') in = -1;
   }
   if(im>=0)
   {
     if(s.charAt(im)=='P') jn =  1;
     if(s.charAt(im)=='N') jn = -1;
   }
   if(il>=0)
   {
     if(s.charAt(il)=='P') kn =  1;
     if(s.charAt(il)=='N') kn = -1;
   }
   s = fo.toString();
   il = s.length() - 1;
   im = il - 1;
   ih = im - 1;
   int io = 0;
   int jo = 0;
   int ko = 0;
   if(ih>=0)
   {
     if(s.charAt(ih)=='P') io =  1;
     if(s.charAt(ih)=='N') io = -1;
   }
   if(im>=0)
   {
     if(s.charAt(im)=='P') jo =  1;
     if(s.charAt(im)=='N') jo = -1;
   }
   if(il>=0)
   {
     if(s.charAt(il)=='P') ko =  1;
     if(s.charAt(il)=='N') ko = -1;
   }
   s = fp.toString();
   il = s.length() - 1;
   im = il - 1;
   ih = im - 1;
   int ip = 0;
   int jp = 0;
   int kp = 0;
   if(ih>=0)
   {
     if(s.charAt(ih)=='P') ip =  1;
     if(s.charAt(ih)=='N') ip = -1;
   }
   if(im>=0)
   {
     if(s.charAt(im)=='P') jp =  1;
     if(s.charAt(im)=='N') jp = -1;
   }
   if(il>=0)
   {
     if(s.charAt(il)=='P') kp =  1;
     if(s.charAt(il)=='N') kp = -1;
   }
   String sb = B.toString();
   int bl = sb.length() - 1;
   int bm = bl - 1;
   int bh = bm - 1;
   int ib = 0;
   int jb = 0;
   int kb = 0;
   if(bh>=0)
   {
     if(s.charAt(bh)=='P') ib =  1;
     if(s.charAt(bh)=='N') ib = -1;
   }
   if(bm>=0)
   {
     if(s.charAt(bm)=='P') jb =  1;
     if(s.charAt(bm)=='N') jb = -1;
   }
   if(bl>=0)
   {
     if(s.charAt(bl)=='P') kb =  1;
     if(s.charAt(bl)=='N') kb = -1;
   }
   s = A.toString();
   il = s.length() - 1;
   im = il - 1;
   ih = im - 1;
   int r = 0;
   if(ih>=0)
   {
     switch(s.charAt(ih))
     {
       case 'N': 
         switch(ib)
         {
           case -1: r+=9*in; break;
           case  0: r+=9*io; break;
           case  1: r+=9*ip; break;
         }
         break;
       case 'O':
         switch(ib)
         {
           case -1: r+=9*jn; break;
           case  0: r+=9*jo; break;
           case  1: r+=9*jp; break;
         }
         break;
       case 'P':
         switch(ib)
         {
           case -1: r+=9*kn; break;
           case  0: r+=9*ko; break;
           case  1: r+=9*kp; break;
         }
         break;
     }
   }
   if(im>=0)
   {
     switch(s.charAt(im))
     {
       case 'N': 
         switch(jb)
         {
           case -1: r+=3*in; break;
           case  0: r+=3*io; break;
           case  1: r+=3*ip; break;
         }
         break;
       case 'O':
         switch(jb)
         {
           case -1: r+=3*jn; break;
           case  0: r+=3*jo; break;
           case  1: r+=3*jp; break;
         }
         break;
       case 'P':
         switch(jb)
         {
           case -1: r+=3*kn; break;
           case  0: r+=3*ko; break;
           case  1: r+=3*kp; break;
         }
         break;
     }
   }
   if(il>=0)
   {
     switch(s.charAt(il))
     {
       case 'N': 
         switch(kb)
         {
           case -1: r+=in; break;
           case  0: r+=io; break;
           case  1: r+=ip; break;
         }
         break;
       case 'O':
         switch(kb)
         {
           case -1: r+=jn; break;
           case  0: r+=jo; break;
           case  1: r+=jp; break;
         }
         break;
       case 'P':
         switch(kb)
         {
           case -1: r+=kn; break;
           case  0: r+=ko; break;
           case  1: r+=kp; break;
         }
         break;
     }
   }
   A = new Ternary(r);
 } 

 void signA()
 {
   int ai = A.get();
   if(ai  > 0) rsf =  1;
   if(ai == 0) rsf =  0;
   if(ai  < 0) rsf = -1;
 }

 public int step()
 {
   int i,j,k,st=1;
   String s;
   Ternary tt, t = get(PC++);
   switch(t.get())
   {
     case Ternary.NNN: setN(A); break; // SAN
     case Ternary.NNO: setO(A); break; // SAO
     case Ternary.NNP: setP(A); break; // SAP
     case Ternary.NON: setF(A); break; // SAF
     case Ternary.NOO: setD(PC); break; // SPCD
     case Ternary.NOP: setB(A); break; // SAB
     case Ternary.NPN: setL(A); break; // SAL
     case Ternary.NPO: setM(A); break; // SAM
     case Ternary.NPP: setH(A); break; // SAH

     case Ternary.ONN: // RLA
       tt = new Ternary(A.get()*3+bcf);
       s = tt.toString();
       i = s.length()-3;
       if(i<0) bcf = 0;
       else
       {
         switch(s.charAt(i))
         {
           case 'N': bcf = -1; break;
           case 'O': bcf =  0; break;
           case 'P': bcf =  1; break;
         }
       }
       A = getTriad(s,i+3);
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.ONO: // ADD
       tt = new Ternary(A.get()+B.get()+bcf);
       s = tt.toString();
       i = s.length()-3;
       if(i<0) bcf = 0;
       else
       {
         switch(s.charAt(i))
         {
           case 'N': bcf = -1; break;
           case 'O': bcf =  0; break;
           case 'P': bcf =  1; break;
         }
       }
       A = getTriad(s,i+3);
       signA();
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.ONP: // RRA
       j = 0;
       s = A.toString();
       switch(s.charAt(s.length()-1))
       {
         case 'N': j = -1; break;
         case 'O': j =  0; break;
         case 'P': j =  1; break;
       }
       A = new Ternary(A.get()/3+bcf*9);
       bcf = j;
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.OON: // LAI #
       A = get(PC++);
       st++;
       send(AlphaListener.A,getA());
       break;

     case Ternary.OOO: // ADI #
       tt = get(PC++);
       tt = new Ternary(A.get()+tt.get()+bcf);
       s = tt.toString();
       i = s.length()-3;
       if(i<0) bcf = 0;
       else
       {
         switch(s.charAt(i))
         {
           case 'N': bcf = -1; break;
           case 'O': bcf =  0; break;
           case 'P': bcf =  1; break;
         }
       }
       A = getTriad(s,i+3);
       signA();
       st++;
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.OOP: // OPA #
       opa(get(PC++));
       signA();
       st++;
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.OPN: // LDI # # #
       i = get(PC++).get();
       j = get(PC++).get();
       k = get(PC++).get();
       setD(i,j,k);
       st+=3;
       notifyD(dpf,getD());
       break;

     case Ternary.OPO: // JMP # # #
       i = get(PC++).get();
       j = get(PC++).get();
       k = get(PC++).get();
       PC = 27*(27*i+j)+k;
       st+=3;
       break;

     case Ternary.OPP: // OPB # # #
       Ternary tn = get(PC++);
       Ternary to = get(PC++);
       Ternary tp = get(PC++);
       opb(tn,to,tp);
       signA();
       st+=3;
       send(AlphaListener.A,getA());
       send(AlphaListener.F,getF());
       break;

     case Ternary.PNN: A=getN(); break; // LAN
     case Ternary.PNO: A=getO(); break; // LAO
     case Ternary.PNP: A=getP(); break; // LAP
     case Ternary.PON: A=getF(); break; // LAF
     case Ternary.POO: PC=getDi(); break; // LPCD
     case Ternary.POP: A=getB(); break; // LAB
     case Ternary.PPN: A=getL(); break; // LAL
     case Ternary.PPO: A=getM(); break; // LAM
     case Ternary.PPP: A=getH(); break; // LAH
   }
   send(AlphaListener.PC,new Ternary(PC));
   return st;
 }

 public String toString()
 {
   boolean ff = false;
   Ternary t;
   String s = "";
   for(int a=MEM_BEG+1;a<MEM_END;a++)
   {
      boolean fprint = false;
      t = get(a);
      if(t.is(Ternary.OOO))
      {
        if(!nearEmpty(a,3)) 
           fprint = true;
      }
      else fprint = true;
      if(fprint)
      {
        if(!ff)
        {
           Ternary tt = new Ternary(a);
           s += tt.toString(9)+": // "+a+"\n";
        }
        s += "   "+t.toString(3)+" // "+t.get()+"\n";
      }
      ff = fprint;
   }
   return s;
 }

 public boolean assemble(String s)
 {
   boolean res = true;
   boolean old_ro = readonly;
   readonly = false;


   readonly = old_ro;
   return res;
 }

 public boolean assembleProgram()
 {
   if(program==null) return false;
   return assemble(program.getText());
 }

 public boolean copyProgram()
 {
   if(program==null) return false;
   return program.setText(toString());
 }

 public boolean cleanProgram()
 {
   if(program==null) return false;
   return program.setText("");
 }

} 
