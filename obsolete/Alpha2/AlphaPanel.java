// AlphaPanel.java - Alexander Shabarshin  12.11.2008
// version 2.0 ( see http://3niti.org )
// based on Alpha1.java (28.10.2008 - 12.11.2008)

// 12.11.2008 (v2.0) - initial version

import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class AlphaPanel extends Panel implements MouseListener
{
 int[] leds;
 int[] swis;
 int[] swix;
 int gStep;
 boolean fstep;
 boolean fsave;
 int SP;
 AlphaCore core;

 public AlphaPanel()
 {
   leds = new int[15];
   swis = new int[15];
   swix = new int[15];
   core = new AlphaCore();
   setBackground(new Color(255,255,255));
   addMouseListener(this);
 }
   
 public void paint(Graphics g)
 {
   int i,x,y,k,w;
   int dx = size().width;
   int dy = size().height;
   int st = dx/18;
   gStep = st;
   x = st/2;
   for(i=0;i<15;i++)
   {
       y = st/2;
       switch(leds[i])
       {
         case -1: g.setColor(new Color(0xFF,0x00,0x00)); break;
         case  0: g.setColor(new Color(0xFF,0xFF,0xFF)); break;
         case  1: g.setColor(new Color(0x00,0xFF,0x00)); break;
       }   
       g.fillOval(x,y,st,st);
       g.setColor(new Color(0,0,0));
       g.drawOval(x,y,st,st);
       y += st + st/2;
       k = x + st/4;
       w = st/2;
       g.setColor(new Color(255,255,255));    
       g.fillRect(k,y,w,st);
       y--;
       g.setColor(new Color(0,0,0));
       g.drawRect(k,y,w,st);
       y++;
       switch(swis[i])
       {
         case -1: g.fillRect(k,y+2*st/3,w,st/3); break;
         case  0: g.fillRect(k,y+st/3,w,st/3); break;
         case  1: g.fillRect(k,y,w,st/3); break;
       }
       swix[i] = k;
       x += st;
       if((i%3)==2) x+= st/2;
   }
 }

 public void update(Graphics g)
 {
   paint(g);
 }

 public void run()
 {
        String s = "";
        Ternary t = new Ternary(swis[11],swis[10],swis[9]);
        int aa = 0;
        int k = 1;
        for(int j=8;j>=0;j--)
        {
          aa += swis[j]*k;
          k *= 3;
        }
        switch(swis[12])
        {
           case -1: s=get(aa).toString(); break;
           case  0: s=get(core.getPC()).toString(); break;
           case  1: set(aa,t);s=t.toString(); break;
        }
        k = s.length();
        for(int i=0;i<3;i++)
        {
           if(i < 3-k) leds[9+i] = 0;
           else
           {
              switch(s.charAt(i-3+k))
              {
                case 'N': leds[9+i] = -1; break;
                case 'O': leds[9+i] =  0; break;
                case 'P': leds[9+i] =  1; break;
              }
           }
        }
        t = new Ternary(core.getPC());
        System.out.println("PC="+core.getPC()+" ("+t+")");
        s = t.toString();
        k = s.length();
        for(int j=0;j<9;j++)
        {
           if(j < 9-k) leds[j] = 0;
           else
           {
              switch(s.charAt(j-9+k))
              {
                case 'N': leds[j] = -1; break;
                case 'O': leds[j] =  0; break;
                case 'P': leds[j] =  1; break;
              }
           }
        }
        switch(swis[13])
        {
           case -1: if(fsave){
                    fsave=false;
                    core.setPC(SP);
                    SP=0;
                    } 
                    break;
           case  1: if(!fsave){
                    fsave=true;   
                    SP=core.getPC();
                    core.setPC(aa);
                    }
                    break;
        }
        switch(swis[14])
        {
           case -1: step(); 
                    break;
           case  0: fstep=false; 
                    break;
           case  1: if(!fstep){
                    step();
                    fstep=true;};
                    break;
        }
        s = core.getA().toString();
        k = s.length();
        for(int i=0;i<3;i++)
        {
           if(i < 3-k) leds[12+i] = 0;
           else
           {
              switch(s.charAt(i-3+k))
              {
                case 'N': leds[12+i] = -1; break;
                case 'O': leds[12+i] =  0; break;
                case 'P': leds[12+i] =  1; break;
              }
           }
        }
        repaint();
 }

 public void mousePressed(MouseEvent e)
 {
 }

 public void mouseReleased(MouseEvent e)
 {
 }

 public void mouseEntered(MouseEvent e)
 {
 }

 public void mouseExited(MouseEvent e)
 {
 }

 public void mouseClicked(MouseEvent e)
 {
   int i;
   int x = e.getX();
   int y = e.getY();
   int st2 = gStep/2;
   int y0 = gStep*2;
   int y1 = y0+st2;
   int y2 = y0+gStep;
   if(y > y0 && y < y1)
   {
     for(i=0;i<15;i++)
     {
       if(x > swix[i] && x < swix[i]+st2)
       {
          if(swis[i] < 1) swis[i]++;
          break;
       }
     }
   }
   if(y > y1 && y < y2)
   {
     for(i=0;i<15;i++)
     {
       if(x > swix[i] && x < swix[i]+st2)
       {
          if(swis[i] > -1) swis[i]--;
          break;
       }
     }
   }
 }

 public Ternary get(int a)
 {
   Ternary t = core.get(a);
   System.out.println("get "+a+" -> "+t);
   return t;
 }

 public void set(int a, Ternary t)
 {
   System.out.println("set "+a+" <- "+t);
   core.set(a,t);
 }

 public int step()
 {
   System.out.println("step "+get(core.getPC()));
   core.step();
   return 1;
 }

 public AlphaCore core()
 {
   return core;
 }

} 
